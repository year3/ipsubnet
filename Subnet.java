import java.util.Scanner;

public class Subnet {

    // Example for type input: 192.168.1.204/24
    // 192.168.1.50/30

    public static void main(String[] args) {
        Scanner Input = new Scanner(System.in);
        System.out.print("Input ip: ");
        String IPAddress = Input.next();
        calculateAddress(IPAddress);
    }

    public static void calculateAddress(String ip4) {
        String[] str = ip4.split("/");
        String ip = str[0];
        String CIDR = str[1];
        String[] str2 = ip.split("\\.");
        int M = Integer.parseInt(CIDR); //mask
        int classedtype = Integer.parseInt(str2[0]);
        if (classedtype > 0 && classedtype <= 127) {
            System.out.println("Class A");
        } else if (classedtype > 127 && classedtype <= 191) {
            System.out.println("Class B");
        } else if (classedtype > 191 && classedtype <= 223) {
            System.out.println("Class C");
        } else if (classedtype > 223 && classedtype <= 239) {
            System.out.println("Class D");
        } else if (classedtype > 239 && classedtype <= 255) {
            System.out.println("Class E");
        }
        if (M > 0 && M <= 8) {
            M = cal(8 - M);
            int Wildcard = 255 - M;
            int temp = Integer.parseInt(str2[0]);
            int bc = temp + Wildcard;
            if (bc >= 255) {
                bc = 255;
            }
            int na = bc - Wildcard;
            System.out.println("Subnet = " + M + ".0.0.0");
            System.out.println("Wildcard = " + Wildcard + ".255.255.255");
            System.out.println("Network address = " + na + ".0.0.0");
            System.out.println("Broadcast address = " + bc + ".255.255.255");
            System.out.println("HostMin = " + na + ".0.0.1");
            System.out.println("HostMax = " + bc + ".255.255.254");
        } else if (M > 8 && M <= 16) {
            M = 16 - M;
            cal(M);
            M = cal(M);
            int Wildcard = 255 - M;
            int temp = Integer.parseInt(str2[1]);
            int bc = temp + Wildcard;
            if (bc >= 255) {
                bc = 255;
            }
            int na = bc - Wildcard;
            System.out.println("Subnet = " + "255." + M + ".0.0");
            System.out.println("Wildcard = 0." + Wildcard + ".255.255");
            System.out.println("Network address = " + str2[0] + "." + na + ".0.0");
            System.out.println("Broadcast address = " + str2[0] + "." + bc + ".255.255");
            System.out.println("HostMin = " + str2[0] + "." + na + ".0.1");
            System.out.println("HostMax = " + str2[0] + "." + bc + ".255.254");
        } else if (M > 16 && M <= 24) {
            M = 24 - M;
            M = cal(M);
            int Wildcard = 255 - M;
            int temp = Integer.parseInt(str2[2]);
            int bc = temp + Wildcard;
            if (bc >= 255) {
                bc = 255;
            }
            int na = bc - Wildcard;
            System.out.println("Subnet = " + "255.255." + M + ".0");
            System.out.println("Wildcard = 0.0." + Wildcard + ".255");
            System.out.println("Network address = " + str2[0] + "." + str2[1] + "." + na + ".0");
            System.out.println("Broadcast address = " + str2[0] + "." + str2[1] + "." + bc + ".255");
            System.out.println("HostMin = " + str2[0] + "." + str2[1] + "." + na + ".1");
            System.out.println("HostMax = " + str2[0] + "." + str2[1] + "." + bc + ".254");
        } else if (M > 24 && M <= 32) {
            M = 32 - M;
            M = cal(M);
            int Wildcard = 255 - M;
            int temp = Integer.parseInt(str2[3]);
            int bc = temp + Wildcard;
            if (bc >= 255) {
                bc = 255;
            }
            int na = bc - Wildcard;
            System.out.println("Subnet = " + "255.255.255." + M);
            System.out.println("Wildcard = 0.0.0." + Wildcard);
            if (M == 255) {
                System.out.println("Network address = " + ip);
                System.out.println("Broadcast address = " + ip);
                System.out.println("HostMin = " + str2[0] + "." + str2[1] + "." + str2[2] + "." + (na + 1));
                System.out.println("HostMax = " + str2[0] + "." + str2[1] + "." + str2[2] + "." + str2[3]);
            } else {
                System.out.println("Network address = " + str2[0] + "." + str2[1] + "." + str2[2] + "." + na);
                System.out.println("Broadcast address = " + str2[0] + "." + str2[1] + "." + str2[2] + "." + bc);
                System.out.println("HostMin = " + str2[0] + "." + str2[1] + "." + str2[2] + "." + (na + 1));
                System.out.println("HostMax = " + str2[0] + "." + str2[1] + "." + str2[2] + "." + (bc - 1));
            }
        } else {
            System.out.println("Wrong ip!");
        }
    }

    public static int cal(int mask) {
        double Calculatesub = 0;
        for (int i = 7; i >= mask; i--) {
            Calculatesub = Calculatesub + Math.pow(2, i);
        }
        int ans = (int) Calculatesub;
        return ans;
    }

}
